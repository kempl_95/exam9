use forum;

CREATE TABLE users (
	id INT NOT NULL AUTO_INCREMENT primary key,
	login varchar(128),
    email varchar(128),
    password varchar(128),
    name varchar(128),
    surname varchar(128)
);
CREATE TABLE subjects (
	id INT NOT NULL AUTO_INCREMENT primary key,
    title varchar(128),
    description varchar(512),
    datetime datetime
);
CREATE TABLE comments (
	id INT NOT NULL AUTO_INCREMENT primary key,
    text varchar(255),
    user_id int,
    INDEX IDX_user (user_id),
	FOREIGN KEY FK_user (user_id)
	REFERENCES users (id)
	ON DELETE RESTRICT
	ON UPDATE CASCADE,
    subject_id int,
    INDEX IDX_subject (subject_id),
	FOREIGN KEY FK_subject (subject_id)
	REFERENCES subjects (id)
	ON DELETE RESTRICT
	ON UPDATE CASCADE
);
alter table subjects
    add column comment_id int after datetime,
    add index IDX_comment (comment_id),
    add FOREIGN KEY FK_comment (comment_id) REFERENCES comments (id);



-- Примеры insert:
-- insert into hw34t2.room values (1, 1), (2, 2);
-- insert into hw34t2.room_client values (1, 'Виктор'),(2, 'Анна'),(3, 'Николай'),(4, 'Юра'),(5, 'Айбек');
-- insert into hw34t2.room_status values (1, 'доступно'),(2, 'в использовании'),(3, 'забронировано');