use forum;

alter table users
	add column role varchar(64) after surname;

alter table users
	add column enabled boolean after role;



-- Примеры insert:
-- insert into hw34t2.room values (1, 1), (2, 2);
-- insert into hw34t2.room_client values (1, 'Виктор'),(2, 'Анна'),(3, 'Николай'),(4, 'Юра'),(5, 'Айбек');
-- insert into hw34t2.room_status values (1, 'доступно'),(2, 'в использовании'),(3, 'забронировано');