use forum;

alter table subjects
    add column user_id int after datetime,
    add index IDX_user (user_id),
    add FOREIGN KEY FK_user (user_id) REFERENCES users (id);



-- Примеры insert:
-- insert into hw34t2.room values (1, 1), (2, 2);
-- insert into hw34t2.room_client values (1, 'Виктор'),(2, 'Анна'),(3, 'Николай'),(4, 'Юра'),(5, 'Айбек');
-- insert into hw34t2.room_status values (1, 'доступно'),(2, 'в использовании'),(3, 'забронировано');