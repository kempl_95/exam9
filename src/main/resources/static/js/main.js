'use strict';
//--< ============================================================== Константы ====================================================================
const baseUrl = 'http://localhost:9999';
let pageParam;
let clickedProductId = '';

const subjectList = document.getElementById("subject_block");
const pageLinks = document.getElementById("page_links");

function PageParam(first, last, number, size, previous, next) {
    this.first = first;
    this.last = last;
    this.number = number;
    this.size = size;
    this.previous = this.number + previous;
    this.next = this.number + next;
}
function Page(id, className, text) {
    this.id = id;
    this.className = className;
    this.text = text;
}
function Subject(id, title, description, datetime, user) {
    this.id = id;
    this.title = title;
    this.description = description;
    this.datetime = datetime;
    this.user = user;
}
function User(login) {
    this.title = login;
}

function addElement(dom_element, adding_element){
    dom_element.append(adding_element);
}
function createPageLinkElement(page) {
    pageLinks.style.display = 'inline-block';
    let pageLinkElement = document.createElement('span');
    pageLinkElement.id = page.id;
    pageLinkElement.className = "page " + page.className;
    pageLinkElement.innerText = page.text;
    return pageLinkElement;
}
function createSubjectElement(subject) {
    let productElement = document.createElement('tr');
    productElement.innerHTML =
            `<td class="w_60"><a href="/subjects/${subject.id}" class="no_link">${subject.title}</a></td>`+
            `<td class="w_10"><a href="/subjects/${subject.id}" class="no_link">${subject.datetime}</a></td>`+
            `<td class="w_15"><a href="/subjects/${subject.id}" class="no_link">${subject.user.login}</a></td>`+
            `<td class="w_15"><a href="/subjects/${subject.id}" class="no_link"></a></td>`
    ;
    return productElement;
}
function productGet(fetch) {
    fetch.then(data => {
        pageParam = new PageParam(data.first, data.last, data.number,5, data.number-1, data.number+1);
        subjectList.innerHTML = `<table>`+
            `<tr>`+
            `<td class="w_60 p_t_1">Название</td>`+
            `<td class="w_20 p_t_1">Дата создания</td>`+
            `<td class="w_10 p_t_1">Пользователь</td>`+
            `<td class="w_10 p_t_1">кол-во ответов</td>`+
            `</tr>`;

        for (let i = 0; i<data.length; i++){
            addElement(subjectList, createSubjectElement(new Subject(data[i].id, data[i].title, data[i].description, data[i].datetime, new User(data[i].user.login))));
        }
        //  addPageElements
        if (pageParam.first&&!pageParam.last){
            pageLinks.innerHTML = '';
            addElement(pageLinks ,createPageLinkElement(new Page('next_product_page', 'next_page', '>')));
            document.getElementById('next_product_page').addEventListener('click', function () {
                productGet(fetchProducts(pageParam.next));
            });
        } else if (!pageParam.first&&pageParam.last){
            pageLinks.innerHTML = '';
            addElement(pageLinks ,createPageLinkElement(new Page('previous_product_page', 'previous_page', '<')));
            document.getElementById('previous_product_page').addEventListener('click', function () {
                productGet(fetchProducts(pageParam.previous));
            });
        } else if (pageParam.last&&pageParam.first){
            pageLinks.innerHTML = '';
        } else {
            pageLinks.innerHTML = '';
            addElement(pageLinks ,createPageLinkElement(new Page('previous_product_page', 'previous_page', '<')));
            document.getElementById('previous_product_page').addEventListener('click', function () {
                productGet(fetchProducts(pageParam.previous));
            });
            addElement(pageLinks ,createPageLinkElement(new Page('next_product_page', 'next_page', '>')));
            document.getElementById('next_product_page').addEventListener('click', function () {
                productGet(fetchProducts(pageParam.next));
            });
        }
    });
}
function update() {
    pageParam = new PageParam(true, false, 0,5, -1, 1);
    if (pageParam.first&&!pageParam.last){
        addElement(pageLinks ,createPageLinkElement(new Page('next_product_page', 'next_page', '>')));
        document.getElementById('next_product_page').addEventListener('click', function () {
            productGet(fetchProducts(pageParam.next));
        });
    } else if (pageParam.last&&pageParam.first){}
    //checkLocalStorageForUserData();
}

const fetchProducts = async (number) => {
    const productsPath = `${baseUrl}/subjects?page=${number}&size=${pageParam.size}`;
    const data = await fetch(productsPath, {cache: 'no-cache'});
    return data.json();
};
update();