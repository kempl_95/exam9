package kg.exam9.forum.frontend;


import kg.exam9.forum.backend.dto.frontDTO.LoginUserDTO;
import kg.exam9.forum.backend.dto.frontDTO.NewUserDTO;
import kg.exam9.forum.backend.model.Subject;
import kg.exam9.forum.backend.service.CommentService;
import kg.exam9.forum.backend.service.PropertiesService;
import kg.exam9.forum.backend.service.SubjectService;
import kg.exam9.forum.backend.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping
@AllArgsConstructor
public class FrontendController {

    private final UserService userService;
    private final SubjectService subjectService;
    private final CommentService commentService;

    private final PropertiesService propertiesService;

    @GetMapping
    public String root(Model model, Pageable pageable, Principal principal) {
        if (principal != null){
            var userResponse = userService.getUserResponseByEmail(principal.getName());
            model.addAttribute("dto", userResponse);
        }
        model.addAttribute("subjects", subjectService.findPageSubjects(pageable).getContent());

        return "index";
    }
    @GetMapping("/subjects/{id:\\d+?}")
    public String productPage(@PathVariable int id, Model model, Principal principal, HttpSession session) {
        model.addAttribute("subject", subjectService.getSubjectById(id));
        if (principal != null){
            var user = userService.getUserResponseByEmail(principal.getName());
            model.addAttribute("dto", user);
        }
        model.addAttribute("comments", commentService.getCommentsBySubjectId(id));
        return "subject";
    }


    // РЕГИСТРАЦИЯ
    @GetMapping("/registration")
    public String reg() {
        return "registration";
    }

    @PostMapping("/registration")
    public String registerPage(NewUserDTO newUserDTO) {
        userService.addUser(newUserDTO);
        return "redirect:/login";
    }

    @GetMapping("/login")
    public String log() {
        return "login";
    }



}
