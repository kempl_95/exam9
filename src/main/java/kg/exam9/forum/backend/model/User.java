package kg.exam9.forum.backend.model;

import lombok.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;


@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
@Entity
@Table(name="users")
public class User{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(length = 128)
    private String login;

    @Column(length = 128)
    private String email;

    @Column(length = 128)
    private String password;

    @Column(length = 128)
    private String name;

    @Column(length = 128)
    private String surname;

    @Column
    @Builder.Default
    private boolean enabled = true;

    @Column(length = 128)
    @Builder.Default
    private String role = "USER";
}
