package kg.exam9.forum.backend.service;


import kg.exam9.forum.backend.model.Comment;
import kg.exam9.forum.backend.repository.Comments;
import kg.exam9.forum.backend.repository.Subjects;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class CommentService {

    private final Comments comments;


    public List<Comment> getCommentsBySubjectId(int id){
        return comments.findAllBySubjectId(id);
    }

}
