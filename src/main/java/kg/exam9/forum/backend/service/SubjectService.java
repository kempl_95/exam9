package kg.exam9.forum.backend.service;


import kg.exam9.forum.backend.dto.SubjectDTO;
import kg.exam9.forum.backend.exception.ResourceNotFoundException;
import kg.exam9.forum.backend.model.Subject;
import kg.exam9.forum.backend.repository.Subjects;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class SubjectService {

    private final Subjects subjects;

    public Page<SubjectDTO> findPageSubjects(Pageable pageable){
        return subjects.findAll(pageable)
                .map(SubjectDTO::from);
    }
    public SubjectDTO getSubjectById(int id) {
        return SubjectDTO.from(subjects.getById(id));
    }
}
