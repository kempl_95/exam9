package kg.exam9.forum.backend.service;


import kg.exam9.forum.backend.dto.frontDTO.NewUserDTO;
import kg.exam9.forum.backend.dto.frontDTO.UserResponseDTO;
import kg.exam9.forum.backend.exception.UserAlreadyRegisteredException;
import kg.exam9.forum.backend.exception.UserNotFoundException;
import kg.exam9.forum.backend.model.User;
import kg.exam9.forum.backend.repository.Users;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class UserService {

    private final Users users;
    private final PasswordEncoder passwordEncoder;

    public void addUser(NewUserDTO userData) {
        if (users.existsByEmail(userData.getEmail())) {
            throw new UserAlreadyRegisteredException();
        }
        var user = User.builder()
                .name(userData.getName())
                .surname(userData.getSurname())
                .email(userData.getEmail())
                .login(userData.getLogin())
                .password(passwordEncoder.encode(userData.getPassword()))
                .build();

        users.save(user);
    }
    public UserResponseDTO getUserResponseByEmail(String email) {
        var user = users.findByEmail(email)
                .orElseThrow(UserNotFoundException::new);

        return UserResponseDTO.from(user);
    }
    public User getUserByEmail(String email) {
        return users.findByEmail(email)
                .orElseThrow(UserNotFoundException::new);
    }
    public boolean existByEmail(String email){
        return users.existsByEmail(email);
    }
}
