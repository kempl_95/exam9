package kg.exam9.forum.backend.repository;

import kg.exam9.forum.backend.model.Comment;
import kg.exam9.forum.backend.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.Optional;

public interface Comments extends JpaRepository<Comment, Integer> {
    List<Comment> findAllBySubjectId(int id);

}
