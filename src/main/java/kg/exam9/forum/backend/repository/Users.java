package kg.exam9.forum.backend.repository;

import kg.exam9.forum.backend.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import java.util.Optional;

public interface Users extends JpaRepository<User, Integer> {
    Optional<User> findByEmail(String email);
    public boolean existsByEmail(String email);
    public User getByEmail(String s);

}
