package kg.exam9.forum.backend.repository;

import kg.exam9.forum.backend.model.Subject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface Subjects extends JpaRepository<Subject, Integer> {
    Subject getById(Integer id);
}
