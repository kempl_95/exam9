package kg.exam9.forum.backend.controller;

import kg.exam9.forum.backend.dto.SubjectDTO;
import kg.exam9.forum.backend.service.SubjectService;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/subjects")
@AllArgsConstructor(access = AccessLevel.PRIVATE)
class PlaceRestController {
    private final SubjectService subjectService;

    @GetMapping
    public List<SubjectDTO> getPlaces(Pageable pageable) {
        return subjectService.findPageSubjects(pageable).getContent();
    }

}
