package kg.exam9.forum.backend.dto.frontDTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NewUserDTO {
    private String name;
    private String surname;
    private String email;
    private String login;
    private String password;
}
