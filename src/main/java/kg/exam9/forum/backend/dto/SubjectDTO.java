package kg.exam9.forum.backend.dto;

import kg.exam9.forum.backend.dto.frontDTO.UserResponseDTO;
import kg.exam9.forum.backend.model.Comment;
import kg.exam9.forum.backend.model.Subject;
import kg.exam9.forum.backend.model.User;
import lombok.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class SubjectDTO {
    public static SubjectDTO from(Subject subject) {
        return builder()
                .id(subject.getId())
                .title(subject.getTitle())
                .description(subject.getDescription())
                .datetime(LocalDateTime.now())
                .user(UserResponseDTO.from(subject.getUser()))
                .build();
    }
    public static List<SubjectDTO> listFrom(List<Subject> objList){
        List<SubjectDTO> listDto = new ArrayList<>();
        objList.stream().forEach(obj -> {
            listDto.add(SubjectDTO.from(obj));
        });
        return listDto;
    }

    private Integer id;
    private String title;
    private String description;
    private LocalDateTime datetime;
    private UserResponseDTO user;
}
